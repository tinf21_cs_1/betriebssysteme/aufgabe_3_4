public class Erzeuger extends Thread {

    public int nummer;

    public Erzeuger(int nummer) {
        this.nummer = nummer;
    }

    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            Main.semE.down();
            Main.rb.insertItem(i);
            Main.semV.up();
        }
    }
}

