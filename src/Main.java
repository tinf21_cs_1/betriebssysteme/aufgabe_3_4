import util.RingBuffer;
import util.Semaphore;

public class Main {

    public static RingBuffer rb;

    public static Semaphore semE;
    public static Semaphore semV;

    public static void main(String[] args) {

        rb = new RingBuffer(5);
        semE = new Semaphore(5, "ErzeugerSemaphore");
        semV = new Semaphore(0, "VerbraucherSemaphore");

        for (int i = 1; i <= 1; i++) {
            new Erzeuger(i).start();
        }

        for (int i = 1; i <= 1; i++) {
            new Verbraucher(i).start();
        }

        System.out.println(rb.toString());
    }
}