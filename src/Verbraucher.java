public class Verbraucher extends Thread {

    public int nummer;
    public int verbrauch;

    public Verbraucher(int nummer) {
        this.nummer = nummer;
    }

    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            Main.semV.down();
            this.verbrauch = Main.rb.removeItem();
            System.out.println("Verbraucht: " + this.verbrauch);
            Main.semE.up();
        }
    }
}
